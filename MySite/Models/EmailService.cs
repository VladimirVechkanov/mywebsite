﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MimeKit;
using MailKit.Net.Smtp;

namespace SocialApp.Services {
    public class EmailService {
        public async Task SendEmailAsync(string name, string email, string phone, string message) {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Администрация сайта", "tilda.002@yandex.ru"));
            emailMessage.To.Add(new MailboxAddress("Администрация сайта", "wowa.98@mail.ru"));
            emailMessage.Subject = "Письмо с сайта: от " + name + ", EMail: " + email + ", Phone: " + phone;
            emailMessage.Body = new TextPart("Plain") {
                Text = message
            };

            using (var client = new SmtpClient()) {
				try {
					await client.ConnectAsync("smtp.yandex.ru", 25, false);
					await client.AuthenticateAsync("tilda.002@yandex.ru", "002tilda002");
					await client.SendAsync(emailMessage);

					await client.DisconnectAsync(true);
				}
				catch (Exception ex) {
                    var q = ex.Message;
				}
                
            }
        }
    }
}
