﻿
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MySite.Domain.Entities;
using System;

namespace MySite.Domain {
	public class AppDbContext : IdentityDbContext<IdentityUser> {
		public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }
		public DbSet<TextField> TextFields { get; set; }
		public DbSet<ServiceItem> ServiceItems { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder) {
			base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<IdentityRole>().HasData(new IdentityRole {
				Id = "34191421-07f2-4dba-abb3-5607ddfabacc",
				Name = "admin",
				NormalizedName = "ADMIN"
			});

			modelBuilder.Entity<IdentityUser>().HasData(new IdentityUser {
				Id = "03444618-ba37-4e65-8e8f-d1e74646e3b1",
				UserName = "admin",
				NormalizedUserName = "ADMIN",
				Email = "wowa.98@mail.ru",
				NormalizedEmail = "WOWA.98@MAIL.RU",
				EmailConfirmed = true,
				PasswordHash = new PasswordHasher<IdentityUser>().HashPassword(null, "98wolodaserfert89"),
				SecurityStamp = string.Empty
			});

			modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string> {
				RoleId = "34191421-07f2-4dba-abb3-5607ddfabacc",
				UserId = "03444618-ba37-4e65-8e8f-d1e74646e3b1"
			});

			modelBuilder.Entity<TextField>().HasData(new TextField {
				Id = new Guid("69768b06-e978-4877-829d-0a9c443e3bd4"),
				CodeWord = "PageIndex",
				Title = "Главная"
			});

			modelBuilder.Entity<TextField>().HasData(new TextField {
				Id = new Guid("461f1879-130e-46f3-a947-0342f990037f"),
				CodeWord = "PageServices",
				Title = "Мои услуги"
			});

			modelBuilder.Entity<TextField>().HasData(new TextField {
				Id = new Guid("f1e75ba8-a09c-4d44-a3f2-ebe4ad307004"),
				CodeWord = "PageContacts",
				Title = "Контакты"
			});
		}
	}
}
