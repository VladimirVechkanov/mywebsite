﻿using Microsoft.AspNetCore.Mvc;
using MySite.Domain;
using MySite.Models;
using SocialApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MySite.Controllers {
	public class HomeController : Controller {
		private readonly DataManager dataManager;

		public HomeController(DataManager dataManager) {
			this.dataManager = dataManager;
		}
		public IActionResult Index() {
			return View(dataManager.ServiceItems.GetServiceItems());
		}

		[HttpPost]
		public async Task<IActionResult> SendMessage(string name, string email, string phone, string message) {
			EmailService emailService = new EmailService();
			await emailService.SendEmailAsync(name, email, phone, message);
			return Redirect("/");
		}
	}
}
